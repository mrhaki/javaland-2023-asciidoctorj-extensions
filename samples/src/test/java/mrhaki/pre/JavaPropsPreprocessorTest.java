package mrhaki.pre;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class JavaPropsPreprocessorTest {

    @Test
    void javaSysPropsDocumentAttributes() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().preprocessor(JavaPropsPreprocessor.class);
            
            // when
            String result = asciidoctor.convert(
                    "Java version {java-version}",
                    Options.builder()
                           .attributes(Attributes.builder()
                                                 .attribute("java-version", "11")
                                                 .build())
                           .build());
            
            // then
            assertThat(result).contains("Java version 11");
        }
    }
}
