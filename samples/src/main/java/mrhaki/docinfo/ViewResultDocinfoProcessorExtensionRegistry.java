package mrhaki.docinfo;

import mrhaki.post.CustomFooterPostProcessor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry to register {@link ViewResultDocinfoProcessor}.
 */
public class ViewResultDocinfoProcessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().postprocessor(CustomFooterPostProcessor.class);
    }
}
