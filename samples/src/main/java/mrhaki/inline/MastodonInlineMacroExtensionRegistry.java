package mrhaki.inline;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry implementation to register {@link MastodonInlineMacroProcessor}.
 */
public class MastodonInlineMacroExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().inlineMacro(MastodonInlineMacroProcessor.class);
    }
}
