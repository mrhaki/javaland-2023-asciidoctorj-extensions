package mrhaki.block;

import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockProcessor;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.Reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * BlockProcessor to process blocks that start with <code>[bash]</code>.
 * We assume the first line of the block is a bash command.
 * The command is executed and the command together with the output
 * is returned as a new listing block.
 */
@Name("bash")
public class BashBlockProcessor extends BlockProcessor {
    @Override
    public Object process(final StructuralNode parent, final Reader reader, final Map<String, Object> attributes) {
        // Read the first line of the block. 
        // We assume this has the command to execute.
        String firstLine = reader.readLine();

        // Get the output of the command.
        String content = runProcessWithOutput(firstLine);

        // Where we set attribute for the listing block.
        // In Asciidoc markup we would write:
        // [source,bash]
        attributes.put("language", "bash");
        attributes.put("style", "source");

        // Create a new listing block with the command and output.
        return createBlock(parent, "listing", content, attributes);
    }

    private String runProcessWithOutput(String firstLine) {
        // We get the string after the $ which is the actual command we can run.
        String command = firstLine.substring(2);

        // StringBuilder to hold the new content.
        StringBuilder content = new StringBuilder(firstLine);

        try {
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = is.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        } catch (IOException e) {
            content.append("Cannot execute command `")
                   .append(command)
                   .append("`");
        }

        return content.toString();
    }
}
