package mrhaki.block;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry implementation to register {@link BashBlockProcessor}.
 */
public class BashBlockProcessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().block(BashBlockProcessor.class);
    }
}
