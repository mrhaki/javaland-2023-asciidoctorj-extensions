plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.asciidoctorj)
    implementation(libs.asciidoctorj.pdf)
    
    implementation(libs.rome)
    implementation(libs.jsoup)
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

testing {
    suites {
        val test by getting(JvmTestSuite::class) {
            useJUnitJupiter(libs.versions.junit.get())
            
            dependencies {
                implementation(libs.assertj.core)
            }
        }
    }
}
