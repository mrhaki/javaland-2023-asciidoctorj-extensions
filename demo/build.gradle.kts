plugins {
    `java-library`
}

description = "Demo project for Asciidoctorj extensions."
version = libs.versions.app.version.get()

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.asciidoctorj)
    implementation(libs.asciidoctorj.pdf)
    
    implementation(libs.rome)
    implementation(libs.jsoup)
}

testing {
    suites {
        val test by getting(JvmTestSuite::class) {
            useJUnitJupiter(libs.versions.junit)

            dependencies {
                implementation(libs.assertj.core)
            }
        }
    }
}
