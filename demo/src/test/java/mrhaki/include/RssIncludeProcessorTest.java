package mrhaki.include;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class RssIncludeProcessorTest {
    
    @Test
    void generate() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().includeProcessor(RssIncludeProcessor.class);
                    
            // when
            asciidoctor.convertFile(
                    new File("rss.adoc"),
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
    
}
