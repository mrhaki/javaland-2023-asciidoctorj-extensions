package mrhaki.inline;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

class MastodonInlineMacroProcessorTest {

    @Test
    void generate() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convert(
                    """
                            = Test
                            //:mastodon-server: mastodon.social
                            
                            Follow me at mastodon:mrhaki[mastodon.javaland]""",
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build());

            System.out.println(result);
        }
    }
    
}
