package mrhaki.include;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.IncludeProcessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

/**
 * include::rss[feed=https://blog.jdriven.com/atom.xml, entries=20]
 */
public class RssIncludeProcessor extends IncludeProcessor {


    @Override
    public boolean handles(String target) {
        return "rss".equals(target);
    }

    @Override
    public void process(Document document, PreprocessorReader reader, String target, Map<String, Object> attributes) {

        String feedUrl = (String) attributes.get("feed");
        int numberOfEntries = Integer.parseInt((String) attributes.get("entries"));

        String content = getContentFeed(feedUrl, numberOfEntries);

        reader.pushInclude(content, feedUrl, "rss", 1, attributes);
    }
    
    private String getContentFeed(final String feed, final int entries) {
            try {
                SyndFeed syndFeed = new SyndFeedInput().build(new XmlReader(new URL(feed)));
    
                StringBuilder content = new StringBuilder();
    
                syndFeed.getEntries()
                        .stream()
                        .limit(entries)
                        .forEach(entry -> {
                            content.append("* ");
                            content.append(entry.getLink());
                            content.append("[").append(entry.getTitle()).append("]");
                            content.append(System.lineSeparator());
                        });
                
                return content.toString();
            } catch (FeedException | IOException e) {
                return "error getting feed content";
            }
        }
}
