package mrhaki.inline;

import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.PositionalAttributes;

import java.util.HashMap;
import java.util.Map;

/**
 * Follow me at mastodon:mrhaki[]
 *
 * Follow me at mastodon:mrhaki[server=mastodon.social]
 * Follow me at mastodon:mrhaki[mastodon.social]
 */
@Name("mastodon")
@PositionalAttributes({"server"})
public class MastodonInlineMacroProcessor extends InlineMacroProcessor {
    @Override
    public Object process(ContentNode parent, String target, Map<String, Object> attributes) {
        String docServer = (String) parent.getDocument().getAttribute("mastodon-server", "mastodon.online");
        String attrServer = (String) attributes.getOrDefault("server", docServer);
        
        Map<String, Object> options = new HashMap<>();
        options.put("type", ":link");
        options.put("target", "https://" + attrServer + "/@" + target);
        
        return createPhraseNode(parent, "anchor", "@" + target + "@" + attrServer, attributes, options);
    }
}
