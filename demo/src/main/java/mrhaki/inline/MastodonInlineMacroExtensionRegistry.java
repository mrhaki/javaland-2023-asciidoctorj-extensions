package mrhaki.inline;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

public class MastodonInlineMacroExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().inlineMacro(MastodonInlineMacroProcessor.class);
    }
}
