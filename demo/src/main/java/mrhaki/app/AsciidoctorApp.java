package mrhaki.app;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.asciidoctor.ast.Document;

import java.io.File;

public class AsciidoctorApp {

    public static void main(String[] args) {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            Document document = asciidoctor.loadFile(
                    new File("sample.adoc"),
                    Options.builder().safe(SafeMode.UNSAFE).build());
            
            System.out.println("document.title -> " + document.getTitle());
            System.out.println("# blocks -> " + document.getBlocks().size());
            System.out.println("section[0].title -> " + document.getBlocks().get(0).getTitle());
        }
    }
}




























/*
    private static void loadFile() {
        try (Asciidoctor asciidoctor = create()) {
            Document document = asciidoctor.loadFile(
                    new File("sample.adoc"), 
                    Options.builder().safe(SafeMode.UNSAFE).build());

            System.out.println("document.title = " + document.getTitle());
            System.out.println("section[0].title = " + document.getBlocks().get(0).getTitle());
        }
    }

    private static void convertText() {
        try (Asciidoctor asciidoctor = create()) {
            String result = asciidoctor.convert(
                    """
                            Good morning at {conference}.""",
                    Options.builder()
                           .attributes(
                                   Attributes.builder()
                                             .attribute("conference", "JFall 2022")
                                             .build()
                           )
                           .build());

            System.out.println(result);
        }
    }

    private static void convertFile() {
        try (Asciidoctor asciidoctor = create()) {
            asciidoctor.convertFile(
                    new File("sample.adoc"),
                    Options.builder()
                           .backend("html")
                           .toFile(new File("sample-output.html"))
                           .safe(SafeMode.UNSAFE)
                           .attributes(
                                   Attributes.builder()
                                             .attribute("source-highlighter", "highlight.js")
                                             .build()
                           )
                           .build());
        }
    }

 */
